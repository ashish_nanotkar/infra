import base64
import pickle

def pickle_loads( data_str ):
    return pickle.loads(base64.b64decode(data_str))

class FilterModule(object):
    def filters(self):
        return { 'pickle_loads': pickle_loads }
